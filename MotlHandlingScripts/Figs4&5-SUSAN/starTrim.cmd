# Provided a .star file that you can open in Blender using Molecular Nodes (https://github.com/BradyAJohnston/MolecularNodes) 
# This script can help you remove unwanted particles (mask a mitochondrion ; trim particles % Z coordinate...).
# It doesn't run on its own but lists commands that you should feel free to copy-paste-edit in your terminal.
# I usually call input basename "I" and output base name "O".
###########################################################################################

# Grab the data provided header size is 30 lines
awk '{if(NR>30) print $2 " " $3 " " $4}' I.bd.star > I.txt

module load IMOD
point2model -in I.txt -ou I.mod -sc -sphere 5 # -sc makes points scattered ; -sphere 5 helps visualizing particles in 3dmod.
# Use 3dmod Viewer (v to open it; p to highlight selected particle) to fill a "trim.list" text files with unwanted particle numbers in decreasing order.

# Offset particle number >> line number in the .star file.    Header size is still hardcoded as 30 lines.
awk '{print $0+30}' trim.list > trim+30.list  # Star Header size = 30
cpy I.bd.star O.bd.trim.star # bkp
for i in `cat trim+30.list` ; do sed -i "${i}d" O.bd.trim.star ; done

#  *ding!*  You can open O.bd.trim.star in Blender with Molecular Nodes to check your prettified output.
