# Example project file that can be submitted as a python job on your favorite cluster using 
# sbatch -N 1 -t 1-00:00:00 ./B_py.sh pb61.py
 
# SUSAN requires NUMPY and NUMBA
import susan
import numpy as np
import matplotlib.pyplot as plt

# To create masks:
from scipy.ndimage import gaussian_filter as gauss
from scipy.ndimage import binary_dilation as dilat

def create_sphere(radius,box_size):
    N = box_size//2
    t = np.arange(-N,N)
    x,y,z = np.meshgrid(t,t,t)
    rad = np.sqrt( x**2 + y**2 + z**2 )
    return np.float32((radius-rad).clip(0,1))

####################################################

GPUlist = [0,1,2,3]

####################################################

# Initialize STA project at bin4
prj = 'prj_b61'
iniPfile='prj_b6/ite_0040/particles.ptclsraw'
box=96
binning=4
bpIni=17

#IniREF
#(R,g)=(10,2)
#ref=create_sphere(R,box)
#ref=-gauss(np.float32(ref),g)
#refName='sph_b%i_r%ig%i-inv.mrc'%(box,R,g)
#susan.io.mrc.write(ref,'ref/'+refName)
#print(refName)
refPath='' #'ref/'
#refName='prj_021it14AVGb8box48_class001.mrc'
refName='prj_b6-i40_class001.mrc' #'w53-unfil_denoised-sph150r42g3-inv.shrunk4.LP60.mrc'

#IniMSK
mskPath='masks/'
#Sph
(R,g)=(24,2)
M=create_sphere(R,box)
M=gauss(np.float32(M),g)
mskName='sph_b%i_r%ig%i.mrc'%(box,R,g)
susan.io.mrc.write(M,mskPath+mskName)
print(mskName)

# Initialize .refstxt file (looking for ref & mask)

#susan.io.mrc.write(create_cyl(7,8,32,'y'),'mask_cyl_b4y.mrc',2.075*4)
refs = susan.data.Reference(n_refs=1) #Multi-ref moeglich == classification
refs.ref[0] = refPath+refName #iniREF
refs.msk[0] = mskPath+mskName #aliMSK
refs.save(prj+'.refstxt')
print(prj)

mngr = susan.project.Manager(prj,box)
mngr.initial_reference = prj+'.refstxt'
mngr.initial_particles = iniPfile
#mngr.tomogram_file     = 'tomos_b%i.tomostxt'%binning
mngr.tomogram_file     = 'tomos_b%i-SCRATCH.tomostxt'%binning
mngr.list_gpus_ids     = GPUlist
mngr.aligner.ctf_correction  = 'cfsc' # on_reference/cfsc
mngr.aligner.allow_drift     = False
mngr.aligner.extra_padding  = 0 # !!! effective box size 48+32=80 during alignment => LP very low
mngr.averager.ctf_correction = 'wiener' # Wiener
mngr.averager.extra_padding  = 0 # because box is too small at bin8
mngr.cc_threshold   = 0.9
mngr.fsc_threshold  = 0.5
mngr.iteration_type = 3 # 3D Alignment
mngr.verbosity = 1
# print('Aligner box size = %ipx'%(mngr.box_size+mngr.aligner.extra_padding))
# print('Averager box size = %ipx'%(mngr.box_size+mngr.averager.extra_padding))

mngr.aligner.bandpass.lowpass  = bpIni # 10fpx @b64 = LP53A

mngr.aligner.set_angular_search(60,6,60,6) # coneSearch(range),coneStep,inplaneSearch,inplaneStep
mngr.aligner.refine.levels = 4
mngr.aligner.refine.factor = 2
mngr.aligner.set_offset_search(4) # PIXELS! in the reference space (:
                                  # ccSphereSize, ccSamplingStep (default = 1px step)

#############################################block1
# if i < 6:
for i in range(1,11):
    bp = mngr.execute_iteration(i)

##############################################block2
bp=bpIni
mngr.aligner.set_angular_search(20,5,20,5)
mngr.aligner.refine.levels = 4
mngr.aligner.refine.factor = 2
mngr.aligner.bandpass.lowpass  = bp-2

# if i > 5:
for i in range(11,21):
    bp = mngr.execute_iteration(i)

##############################################block3   # Careful: prone to overfitting. Latest output is probably not the best one.
mngr.aligner.set_angular_search(20,4,20,4)
lp=bp-2
for i in range(21,41):
    mngr.aligner.bandpass.lowpass = lp
    bp = mngr.execute_iteration(i)
    lp = bp-2 #bp-1 too little ; bp-4 too much. go for lp=bp-2


