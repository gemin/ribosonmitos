#!/bin/bash
#SBATCH -p gpu-el8 #queue where our gpu machines are 
#SBATCH -C gpu=3090 #type of gpu you want (optional)
#SBATCH --gpus 6 #ask for num gpus
#SBATCH --cpus-per-gpu 8 #ask for num cpus per gpu
#SBATCH --mem-per-gpu 24G #minimum memory allocated to job per gpu
#SBATCH -A mattei                   # group to which you belong
#SBATCH -o slurm.%N.%j.out          # STDOUT
#SBATCH -e slurm.%N.%j.err          # STDERR
#wut? --ntasks-per-gpu num #ask for num tasks per allocated gpu

source ~/Miniconda_init.sourceMe # just to initialize conda. This bit might be in your .bashrc but I was advised to move it to a separate file that I could source whenever needed instead of having conda initialized by default and messing up with VNCviewer. Sorry for the rant. (:
conda activate local_susan_p3.9.5
#export CUDA_VISIBLE_DEVICES="0,1,2,3,4,5,6,7"   # nope : slurm does it for you.
python $1
