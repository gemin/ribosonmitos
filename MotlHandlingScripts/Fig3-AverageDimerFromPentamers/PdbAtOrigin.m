%% PdbatOrigin.m
% Quaternions describe only rotations, not translations.
% To get proper atom coordinates after a complex transformation,
%      1.move object to origin (-offset),
%      2.rotate,
%      3.add custom offset (also rotated).
% This script realizes point 1.

% User input
I = 'Day0_LSU.pdb';
O = [I(1:end-4) '_atOrigin.pdb'];

% Yallah habibi
PDBdata = pdb2mat(I);
% Cite as: Evan (2023). read and write PDB files using matlab (https://www.mathworks.com/matlabcentral/fileexchange/42957-read-and-write-pdb-files-using-matlab), MATLAB Central File Exchange. Retrieved June 26, 2023. 

offset=[mean(PDBdata.X) ; mean(PDBdata.Y) ; mean(PDBdata.Z)];

% > origin
PDBdata.X=PDBdata.X-offset(1);
PDBdata.Y=PDBdata.Y-offset(2);
PDBdata.Z=PDBdata.Z-offset(3);

PDBdata.outfile=O;
mat2pdb(PDBdata);
