function [newmotl,indeces_good5,motl_good5]=SM_find_pentamers_normal(motlname,virusnum,m_parameter,threshold,offset,WriteFlag)

% A script to find the pentameric arrangement within a virus motl 
% The pentamers will be positioned at the centre of mass of the neighbors.
% The pentamer z axis is normal to the plane fitting the neighboring 
% postions. The z axis points out from the core. An offset between the 
% plane and the final position of the pentamer along the normal
% can be choosen.
%  
% Usage:
% motlname='motl_26_cleaned_by_distance_mito_0';
% virusnum=7;
% m_parameter='5mer_parameter_M14.em';
% threshold=2;
% offset=0;
% [newmotl]=SM_find_pentamers_normal(motlname,virusnum,m_parameter,threshold,offset)
%% Read motl
mot_list = emread([motlname,num2str(virusnum),'.em']);

%% Preliminary variables
xyz_coords = mot_list(8:10,:)+mot_list(11:13,:); xyz_coords = xyz_coords'; %get xyz coordinates
angles = mot_list(18:19,:); angles=angles'; %get angles
tomo=mot_list(5,1);

pairwise_dist = squareform(pdist(xyz_coords)); %calculate distance between every two dots

dist=[];

szall = size(pairwise_dist,1); %number of all coordinates

for i=1:szall
    [sorted_i,ind_i] = sortrows(pairwise_dist(i,:)');
    dist(i,:, 1) = sorted_i;
    dist(i,:, 2) = ind_i';
end

m=emread(m_parameter);% ! M-parameter

allcomb5=[]; 
%get all combinations of 10 nearest neighbourghs for each point
for i=1:szall
    allcomb5 = [allcomb5; nchoosek(dist(i, 1:10, 2),5)];
end

%%for each combination calculate the norm and variation from ideal one
resDiag5=[]; 
for i=1:size(allcomb5,1)
    curVec = allcomb5(i,:);
    vecP = pairwise_dist(curVec, curVec);
    vecP = sortrows(vecP(find(vecP)));
    resDiag5 = [resDiag5; i norm(vecP-m) std(vecP-m)];
end

%% sort on variation
resSorted5 = sortrows(resDiag5,3);
[i,j] = find(resSorted5(:,3)<threshold);

%% get good indeces
indeces_good5 = allcomb5(resSorted5(i,1),:);
if size(indeces_good5,2)>0
    sort_indeces_good5 = sort(indeces_good5')';
    [b1, ind, n1] = unique(sort_indeces_good5,'rows');
    indeces_good5 = indeces_good5(ind,:);
end

motl_good5=mot_list(:,indeces_good5');

%fill a mot_list
motl_list2 = [];

for i=1:size(indeces_good5,1)
    centerCrd=sum(xyz_coords(indeces_good5(i,:),:))/5;
    
    % Fit a plane through the neighbors
    XYZ = xyz_coords(indeces_good5(i,:),:); % coordinates of the 5 neighbors
    
    x=XYZ(:,1);
    y=XYZ(:,2);
    z=XYZ(:,3);

    % The following code comes from the matlab forum and will find the plane that fits the position of the 5 neighbors
    A =[x,y,z,ones(length(x),1)];
    [U,S,V] = svd(A);
    ss=diag(S);
    q=find(ss==min(ss)); % find position of minimal singular value
    coeff=V(:,min(q));
    coeff=coeff/norm(coeff(1:3),2);

    % Unit vector normal to the fitted plane
    V1=[coeff(1:3)]';

    % Euler angles to have the pentamer normal to the fitted plane
    theta = 180/pi*atan2(sqrt(V1(1).^2+V1(2).^2),V1(3));
    psi = 180/pi*atan2(V1(2),V1(1)) + 90;

    % Assign the centre coordinates and the Euler angles to the pentamers motl
    newCol=zeros(20,1);
    newCol(8:10)=centerCrd';
    newCol(18) = psi;
    newCol(19) = theta;
    newCol(20)=1;
        
    % Check if the normal is in the right direction
    x_mean = mean(xyz_coords(:,1));     % x y and z of the centre of the core
    y_mean = mean(xyz_coords(:,2));
    z_mean = mean(xyz_coords(:,3));
    cent = [x_mean,y_mean,z_mean];

    centre_norm = normalvec(newCol, cent);  % normal vector of the pentamer position from the centre of the core

    phi = centre_norm(17,1);
    psi = centre_norm(18,1);
    theta = centre_norm(19,1);
    
    % rotation matrix of the centre_normal vector
    rotmat_centre = [cosd(psi) -sind(psi) 0;sind(psi) cosd(psi) 0;0 0 1]*[1 0 0;0 cosd(theta) -sind(theta);0 sind(theta) cosd(theta)]*[cosd(phi) -sind(phi) 0;sind(phi) cosd(phi) 0;0 0 1];

    % euler angles of the pentamer vector that is normal to the fitted plane through the neighbors   
    phi_pent = newCol(17);
    psi_pent = newCol(18);
    theta_pent = newCol(19);

    rotmat = [cosd(psi_pent) -sind(psi_pent) 0;sind(psi_pent) cosd(psi_pent) 0;0 0 1]*[1 0 0;0 cosd(theta_pent) -sind(theta_pent);0 sind(theta_pent) cosd(theta_pent)]*[cosd(phi_pent) -sind(phi_pent) 0;sind(phi_pent) cosd(phi_pent) 0;0 0 1];
    
    % Relative orientation of the two normal vectors 
    relative_orientation = rotmat_centre'*rotmat;
        
    [phi_rot,theta_rot,psi_rot]=dynamo_matrix2euler_simo(relative_orientation);
        
    % If theta is bigger that 90 degrees flip the normal vector to the fitted plane 
    if (theta_rot >= 90 || theta_rot <= -90);
        V1 = -V1;
        theta = 180/pi*atan2(sqrt(V1(1).^2+V1(2).^2),V1(3));
        psi = 180/pi*atan2(V1(2),V1(1)) + 90;
        newCol(18) = psi;
        newCol(19) = theta;
    end
    
    % Calculate phi of the pentamer to be aligned along x
    
    phi_pent = newCol(17);% Vecor with origin on the pentamer position and pointing to the first of the hexamers
    psi_pent = newCol(18);
    theta_pent = newCol(19);
    
    rotmat_pent =  [cosd(psi_pent) -sind(psi_pent) 0;sind(psi_pent) cosd(psi_pent) 0;0 0 1]*[1 0 0;0 cosd(theta_pent) -sind(theta_pent);0 sind(theta_pent) cosd(theta_pent)]*[cosd(phi_pent) -sind(phi_pent) 0;sind(phi_pent) cosd(phi_pent) 0;0 0 1];
    
    % Determine this angle for each neighbor and make an average
    for b = 1:size(XYZ,1);
    vector_pent_hex = XYZ(b,:)-newCol(8:10)';
    
    vector_pent_hex_rotated = vector_pent_hex*rotmat_pent;
    phi(b) = 180/pi*(atan2(vector_pent_hex_rotated(1,2),vector_pent_hex_rotated(1,1)));
    if phi(b) < 0;
        phi(b) = phi(b)+360;
    end
    phi(b) = phi(b)-(floor(phi(b)/72)*72);
    end
    newCol(17) = (mean(phi));
    
    % Add an offset to the pentamer position along the normal
    Voff = [0 0 offset];
    shift_pent = Voff*rotmat_pent';
    newCol(8:10)=newCol(8:10)+shift_pent';
        
    motl_list2=cat(2,motl_list2,newCol);
end


if size(motl_list2,2)>0
    newmotl=motl_list2;
    newmotl(3,:)=mot_list(3,1);
    newmotl(4,:)=1:1:size(newmotl,2);
    newmotl(5,:)=tomo;
    newmotl(6,:)=mot_list(6,1);
    newmotl(7,:)=mot_list(7,1);
    %emwrite(newmotl,['pentamer_virus_',num2str(virusnum),'.em']);
    if WriteFlag == 1
        emwrite(newmotl,['5mer_mito_',num2str(virusnum),'_t',num2str(threshold),'.em']);
    end
else
    newmotl=[];
    disp(['!!! No pentamer found !!! Number:  ------- ',num2str(virusnum),' !!!'])
end
