%% PdbAfterFit2Map.m

% Fit molecule Day0_fullribo.pdb (#13) to map w53-unfil_denoised-sph150r42g3.mrc using 173989 atoms
%   average map value = 0.01152, steps = 248
%   shifted from previous position = 26.7
%   rotated from previous position = 56.7 degrees
%   atoms outside contour = 6172, contour level = 0.0047051
% Position of Day0_fullribo.pdb (#13) relative to w53-unfil_denoised-sph150r42g3.mrc (#7) coordinates:
%   Matrix rotation and translation 
%      0.68722163   0.68605906   0.23885018 -177.23571777
%     -0.70449696   0.54918799   0.44952930 199.36660182
%      0.17723000  -0.47719548   0.86074038 136.83803329
%   Axis  -0.55419404   0.03684971  -0.83157144
%   Axis point  88.14428764 305.16525298   0.00000000
%   Rotation angle (degrees)  56.73069329 
%   Shift along axis  -8.22102053

R = [   0.68722163   0.68605906   0.23885018 ; -0.70449696   0.54918799   0.44952930 ; 0.17723000  -0.47719548   0.86074038];
t = [-177.23571777 199.36660182 136.83803329];


% sstr=['Day0_SSU.pdb'];
% lstr=['Day0_LSU.pdb'];
% inputFiles = {sstr;lstr};
inputFiles = {'Day0_fullribo.pdb'};

for f=1:size(inputFiles,1)
    I = inputFiles{f};
    disp('Input:')
    disp(I)
    O = [I(1:end-4) '_fitted.pdb'];
    %O = [I(1:end-4) '_Q' num2str(K) '.pdb'];
    disp('Output:')
    disp(O)
    % Yallah habibi
    PDBdata = pdb2mat(I);
    
    % rotation
    for i = 1:length(PDBdata.X)
        P=[PDBdata.X(i),PDBdata.Y(i),PDBdata.Z(i)];
        S = R*P'; 
        PDBdata.X(i)=S(1);
        PDBdata.Y(i)=S(2);
        PDBdata.Z(i)=S(3);
    end
    
    % Translation
    PDBdata.X=PDBdata.X+t(1);
    PDBdata.Y=PDBdata.Y+t(2);
    PDBdata.Z=PDBdata.Z+t(3);
    
    % Export pdb
    PDBdata.outfile=O;
    mat2pdb(PDBdata);
end
