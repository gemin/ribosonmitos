function q = euler2quat(phi,psi,theta)

if(nargin==1)
    psi=phi(:,2);
    theta=phi(:,3);
    phi=phi(:,1);
end

q(:,1)=cosd((psi+phi)./2).*cosd(theta./2);
q(:,2)=cosd((psi-phi)./2).*sind(theta./2);
q(:,3)=sind((psi-phi)./2).*sind(theta./2);
q(:,4)=sind((psi+phi)./2).*cosd(theta./2);

normf=repmat(sqrt(sum(q.^2,2)),1,4);
q=q./normf;
