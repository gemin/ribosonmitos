%% PdbQRot.m
% Use this code to read a pdb list of atoms, transform it according to quaternion Q, and output a transformed pdb

inputFiles = {'Day0_SSU_fitted_recentered.pdb';'Day0_LSU_fitted_recentered.pdb';'Day0_fullribo_fitted_recentered.pdb';};

%% Rotation

load('Qtot.mat'); % from pentamerStats.m
load('Q12avg.mat');

% Individual ribo rotations (r_ref=>r_i)
qs=cell(1,5);
for i=1:length(qs)
    qs{i}=Qtot(1,:,i);
end
condStrBase=['_Q'];
incrCountFlag=1;

% Relative ribo rotations (r_ref=>r_i+1|r_i)
qs=cell(1,5);
for i=1:length(qs)
    qs{i}=Qtot(4,:,i);
end
condStrBase=['_Q12-'];
incrCountFlag=1;

% Average ribo rotation
qs={Q12avg};
condStr=['_Q12avg'];
incrCountFlag=0;

%% Translation
addonStr='';
load('Tavg.mat'); % from pentamerStats.m
pxs=4.150; % pdb in Angstroem but Tomo data 4.15 A/px
%pxs=3.7; % come togetheer  oo-oooh      over me
pxs=3.9; % Non-colliding SSUs in atoms view.

% Translating pdb output along a vector in the global frame
transposeFlag=1;
    T=Tavg*pxs;
    if transposeFlag==1; addonStr=[addonStr '_Tavg-pxs' num2str(pxs) 'Apx']; end
    
% Pushing pdb output along a vector in their reference frame
pushFlag=0;
    pushVector=[-150 0 0];
    % pushVector=Tavg;
    pv=pushVector;
    if pushFlag==1
        addonStr=[addonStr '_push' num2str(pv(1)) '_' num2str(pv(2)) '_' num2str(pv(3))];
        %addonStr=['_Tavg'];
    end

%% Los geht's %%
for q=1:length(qs)  %K=1:1
    Q=qs{q};
    if size(Q)==[4,1]
       Q=Q'; 
    end
    
    if incrCountFlag==1
        condStr=[condStrBase num2str(q)];
    end
    %condStr='_Q-1';
%     condStr=['_Q12-' num2str(q)];
%     condStr=['_Q12avg'];
    
    if (transposeFlag==1 || pushFlag==1)
        condStr=[condStr addonStr];
    end    
    
    for f=1:size(inputFiles,1)
        I = inputFiles{f};
        disp('Input:')
        disp(I)
        O = [I(1:end-4) condStr '.pdb'];
        %O = [I(1:end-4) '_Q' num2str(K) '.pdb'];
        disp('Output:')
        disp(O)
        % Yallah habibi
        PDBdata = pdb2mat(I);
        % Cite as: Evan (2023). read and write PDB files using matlab (https://www.mathworks.com/matlabcentral/fileexchange/42957-read-and-write-pdb-files-using-matlab), MATLAB Central File Exchange. Retrieved June 26, 2023.
        
        % Quaternions describe only rotations, not translations => 1.move object to origin (-offset), 2.rotate, 3.resume offset (rotated)
        
%         Trying out v'=qvq' or q'vq  : non conclusive (tried before fitting ribo to STA map)
%         offset=[0;mean(PDBdata.X) ; mean(PDBdata.Y) ; mean(PDBdata.Z)]';
% %         tmp=quat_mult_noNorm(offset,Q');
% %         rot_offset=quat_mult_noNorm(quat_inv(Q),tmp);
%         tmp=quat_mult_noNorm(offset,quat_inv(Q));
%         rot_offset=quat_mult_noNorm(Q',tmp);
%         rot_offset=[rot_offset(2),rot_offset(3),rot_offset(4)];
        
        % > origin
        offset=[mean(PDBdata.X) ; mean(PDBdata.Y) ; mean(PDBdata.Z)];
        PDBdata.X=PDBdata.X-offset(1);
        PDBdata.Y=PDBdata.Y-offset(2);
        PDBdata.Z=PDBdata.Z-offset(3);
        
        % rotation
        
        rot_offset=quat_pointrotate(Q,offset);
        
        for i = 1:length(PDBdata.X)
            P=[PDBdata.X(i),PDBdata.Y(i),PDBdata.Z(i)];
            S = quat_pointrotate(Q,P);
            PDBdata.X(i)=S(1);
            PDBdata.Y(i)=S(2);
            PDBdata.Z(i)=S(3);
            
%         Trying out v'=qvq' or q'vq  : non conclusive (tried before fitting ribo to STA map)   
%             P=[0,PDBdata.X(i),PDBdata.Y(i),PDBdata.Z(i)];
% %             tmp=quat_mult_noNorm(P,Q');
% %             S=quat_mult_noNorm(quat_inv(Q),tmp);
%             tmp=quat_mult_noNorm(P,quat_inv(Q));
%             S=quat_mult_noNorm(Q',tmp);
%             PDBdata.X(i)=S(2);
%             PDBdata.Y(i)=S(3);
%             PDBdata.Z(i)=S(4);
        end
        
        if pushFlag==1
            rot_pushVector=quat_pointrotate(Q,pushVector);
            rot_offset=rot_offset+rot_pushVector;
        end
        
        if transposeFlag==1
            rot_offset=rot_offset+T;
        end
                
        % Resume offset / add push
        PDBdata.X=PDBdata.X+rot_offset(1);
        PDBdata.Y=PDBdata.Y+rot_offset(2);
        PDBdata.Z=PDBdata.Z+rot_offset(3);
        
        % Export pdb
        PDBdata.outfile=O;
        mat2pdb(PDBdata);
    end
end
