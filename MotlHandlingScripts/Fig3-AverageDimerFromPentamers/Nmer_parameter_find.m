#MATLAB R2016b

function Nmer_parameter_find(motl,output)

%% From simo_pentamer_parameter_find.m
% script to find parameters for further pentamer assignment
% Input motl should only contain the n-mer. 

if ischar(motl)
    motl=emread(motl);
end

xyz_coords=motl(8:10,:)+motl(11:13,:);
xyz_coords = xyz_coords';
pairwise_dist = squareform(pdist(xyz_coords));
m=pairwise_dist;
m = sortrows(m(find(m)));
emwrite(m,output);
