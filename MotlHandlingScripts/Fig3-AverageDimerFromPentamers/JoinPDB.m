%% JoinPDB.m

% User input
I1 = 'Day0_LSU.pdb';
I2 = 'Day0_SSU.pdb';
O='Day0_fullribo.pdb';

% Yallah habibi
PDB1 = pdb2mat(I1);
PDB2 = pdb2mat(I2); 

newPDB=PDB1;
f = fieldnames(newPDB);

newPDB.outfile=O;

for i = 2:length(f)
    l1=length(PDB1.(f{i}));
    l2=length(PDB2.(f{i}));
    newPDB.(f{i})(l1+1:l1+l2) = PDB2.(f{i});
 end

mat2pdb(newPDB);
