%% 5merStats.m

% This script grabs series of pentamers summed up in an input motive list
% and outputs the average translation vector Tavg.mat and the average rotation quaternion Q12avg.mat between 2 neighboring ribos in pentamers, 

%% Inputs
motl='/g/mattei/0livier/Warp/53TS-Mitoribo/5mers/motl_11-allPentamers-classSort.em'; % Motive list should have 5merID (1 1 1 1 1 2 2 2 2 2 3 3 3 3 3...) as 20th row to avoid mixing them.
dHistFlag=0; % 1: Plot histogram of distances between ribos within pentamers. The lower mode describes distance between closest neighbors.

%% Yallah
M=emread(motl);


%% [obsolete part] Quickly checking ribo-ribo distances without sorting particle order (shortest distances describe neighbors)
if dHistFlag==1
    n5=max(M(20,:));  % Number of pentamers
    D=zeros(1,10*n5); % Initialize distance data vector
    T=combinations([1 2 3 4 5],2); % Possible permutations
    
    % Index permutation
    perm={};
    for i =1:10
        perm(end+1)={T(i,:)};
    end
    
    % For each pentamer, calculate all possible distances and store them in D
    for i = 0:n5-1
        I=5*i;
        %     P=zeros(3,5);
        %     for c=1:5
        %         P(:,c)=[M(8,I+c)+M(11,I+c);M(9,I+c)+M(12,I+c);M(10,I+c)+M(13,I+c)];
        %     end
        
        for c=1:10
            j = perm{c};
            dX = M(8,I+j(1)) + M(11,I+j(1))  -  M(8,I+j(2)) - M(11,I+j(2)) ;
            dY = M(9,I+j(1)) + M(12,I+j(1))  -  M(9,I+j(2)) - M(12,I+j(2)) ;
            dZ = M(10,I+j(1))+ M(13,I+j(1))  -  M(10,I+j(2))- M(13,I+j(2)) ;
            D(10*i+c)=sqrt( dX*dX + dY*dY + dZ*dZ );
        end
    end
    
    % Smallest distances likely describe closest neighbors within each 5mer...
    dd=sort(D);
    d=dd(1:5*n5);
    
    figure; hist(d)
    figure; hist(dd)
end

%% Actual processing
% For each pentamer:
%   1) Order particles clock-wise
%   Example for pentamer number N:
%     4   5               2   3
%    3     2   becomes   1     4
%       1                   5
RotOrderChkFlag=0; % visual output to debug this part?

%
%   2) Compute translations and rotations (as quaternions to be able to average them : Euler angles can't be averaged.    ...just try.)

m=emread(motl); % Input motl set at beginning of script. Motive list should have 1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 ... as 20th row.
n5=max(m(20,:)); % Number of pentamers
Qtot=zeros(4,4,size(m,2)); % Initialize list of quaternions [q1, q2, q-1, q1>2] (3 first "columns" for debugging; the 4th one is the data we're after)
Ttot=zeros(3,size(m,2));   % Initialize list of translations describing each particle's position.
nullax=zeros(1,size(m,2)); % bin

% Reset shifts to simplify motl
m(8,:)=m(8,:)+m(11,:); m(9,:)=m(9,:)+m(12,:); m(10,:)=m(10,:)+m(13,:);
m(11:13,:)=zeros(size(m(11:13,:)));

for i = 0:n5-1  % Loop over pentamer list
    I=5*i+1; % Individual particle number
    
    % Grab pentamer
    m1=m(:,I:I+4);
    
    % Center of mass
    x=m1(8,:) ; y=m1(9,:) ; z=m1(10,:);
    cm=[mean(x);mean(y);mean(z)];

    %% 1) Re-order pentamer clockwise (when facing OMM, starting from 3 o'clock)
    
    % Fit plane to get a normal vector describing general 5mer orientation
    A =[x',y',z',ones(length(x),1)];
    [U,S,V] = svd(A);
    ss=diag(S);
    q=find(ss==min(ss)); % find position of minimal singular value
    coeff=V(:,min(q));
    coeff=coeff/norm(coeff(1:3),2);
    
    V1=coeff(1:3)'; % Unit vector normal to the fitted plane
    V2=cross(cm,V1); V2=V2/norm(V2);
    V3=cross(V1,V2);
    phi=0; % no need for an in-plane angle since we deal with axes, not particles here
    theta = 180/pi*atan2(sqrt(V1(1).^2+V1(2).^2),V1(3));
    psi = 180/pi*atan2(V1(2),V1(1)) + 90;
 
    %Check if the normal to this plane is well oriented (pointing to or away from mito membrane doesn't matter but they should all be in the same orientation.)
    %1. get rotation matrix describing current pentamer orientation (ZXZ convention for Euler angles)
    rotmat_centre = [cosd(psi) -sind(psi) 0;sind(psi) cosd(psi) 0;0 0 1]*[1 0 0;0 cosd(theta) -sind(theta);0 sind(theta) cosd(theta)]*[cosd(phi) -sind(phi) 0;sind(phi) cosd(phi) 0;0 0 1]; % R_Z*R_X*R_Z 
    %2. get rotation matrix describing the orientation of the first ribo in the pentamer (Z pointing away from mito membrane)
    phi=m1(17,1); theta = m1(19,1); psi = m1(18,1); % #veryConfusing: motl rows ordered in alphabetical order (phi psi theta) instead of ZXZ order (phi theta psi)...............................................
    rotmat_ribo1 = [cosd(psi) -sind(psi) 0;sind(psi) cosd(psi) 0;0 0 1]*[1 0 0;0 cosd(theta) -sind(theta);0 sind(theta) cosd(theta)]*[cosd(phi) -sind(phi) 0;sind(phi) cosd(phi) 0;0 0 1]; % R_Z*R_X*R_Z
    %3. get rotation matrix, then ZXZ angles describing transformation of cm into ribo1
    relative_orientation = rotmat_centre'*rotmat_ribo1;
    [phi_rot,theta_rot,psi_rot]=dynamo_matrix2euler_simo(relative_orientation); % whomever coded this ordered angles properly (ZXZ = phi theta psi = "tdrot tilt narot")
    if (theta_rot >= 90 || theta_rot <= -90)     % If theta is bigger than 90 degrees, flip the normal vector to the fitted plane 
        V2=-V2;
    end
    
    % Order ribosomes in each pentamer using cyl coord % center of mass -- to then select neighbours as [1,2; 2,3; 3,4; 4,5; 5,1].
    globalCoord=[x;y;z];
    localCoord=[x-cm(1);y-cm(2);z-cm(3)];
    projCoord=zeros(2,5);
    cylAngle=zeros(1,5);
    cylR=zeros(1,5);
    for j = 1:5  % Loop over ribo
       projCoord(1,j)=dot(globalCoord(:,j)-cm,V2);
       projCoord(2,j)=dot(globalCoord(:,j)-cm,V3);
       [cylAngle(j),cylR(j)]=cart2pol(projCoord(1,j),projCoord(2,j));       
    end
    
    [~,order]=sort(cylAngle); % Order form -pi to +pi, trigo-wise whenlooking in the direction of the central arrow (= clockwise when facing OMM)
    m(14,I:I+4)=order; %Store ribo order in row 14.
    m1(14,1:5)=order;
    
    %Example for i=0 :                projCoord  
    % p#        1 2 3 4 5               4   5
    % id        3 1 2 5 4              3     2
    %                                     1
    
    if RotOrderChkFlag == 1 && rem(i,40)==0 % Check one pentamer in 40. 1:blue, 2:orange, 3:gold, 4:purple, 5:green.
        figure, hold on; for k=1:5; scatter(projCoord(1,k),projCoord(2,k),'filled'); end
        figure, hold on; for k=1:5; scatter(projCoord(1,order(k)),projCoord(2,order(k)),'x'); end
    end
    
    %% 2) Compute translations and rotations
    % Make quaternions for each _PAIR_ of ribosomes in pentamers (cf euler2quat.m)
    % if q1 == ribo_1 and q2 == ribo_2, we should consider q12 = q2 * quatinv(q1)
    for n = [1,2,3,4,5;2,3,4,5,1] % n will take values [1;2] then [2;3] ... [5;1].
        r1=m1(:,order(n(1))); % ribo N
        r2=m1(:,order(n(2))); % ribo(N+1)
        
        % Rotation
        % q = euler2quat(phi,psi,theta) ; angles input in alphabetical order
        q1 = euler2quat(r1(17),r1(18),r1(19));  % q1i=quat_inv(q1); quat_mult(q1i,q1): % returns 1+0i+0j+0k (:
        q2 = euler2quat(r2(17),r2(18),r2(19));
        q12 = quat_mult(quat_inv(q1),q2); % Note: quaternion product is not commutative
        Qtot(1,:,5*i+n(1))=q1; % for debugging
        Qtot(2,:,5*i+n(1))=q2; % for debugging
        Qtot(3,:,5*i+n(1))=quat_inv(q1); % for debugging
        Qtot(4,:,5*i+n(1))=q12;
        
        % Translation
        p1=r1(8:10); p2=r2(8:10);
        p0=quat_pointrotate(quat_inv(q1),p2-p1);
        Ttot(:,5*i+n(1))=p0;     
    end
end
%Angles
save('Qtot','Qtot'); % Output as Qtot.mat
% Average quaternions :
Q1avg=avg_quaternion_markley(squeeze(Qtot(1,:,:))');
Q2avg=avg_quaternion_markley(squeeze(Qtot(2,:,:))');
Q12avg=avg_quaternion_markley(squeeze(Qtot(4,:,:))');
save('Q12avg','Q12avg'); % Output as Q12avg.mat

%Translations
save('Ttot','Ttot'); % Output as Ttot.mat
figure; quiver3(nullax,nullax,nullax,Ttot(1,:),Ttot(2,:),Ttot(3,:)); % Check that translations are roughly aligned. Should do the same for orientations...
% Average translations :
Tavg=mean(Ttot,2);
save('Tavg','Tavg'); % Output as Tavg.mat

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Notes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%    Q = [0.0354 ; 0.0710 ; -0.0606 ; 0.9950];   for q12 = quat_mult(q2,quat_inv(q1));
% Transform all atom coordinates in pdb => geminied pdb (:


% @ChimeraX

% Fit molecule Day0_fullribo.pdb (#13) to map w53-unfil_denoised-sph150r42g3.mrc using 173989 atoms
%   average map value = 0.01152, steps = 248
%   shifted from previous position = 26.7
%   rotated from previous position = 56.7 degrees
%   atoms outside contour = 6172, contour level = 0.0047051
% Position of Day0_fullribo.pdb (#13) relative to w53-unfil_denoised-sph150r42g3.mrc (#7) coordinates:
%   Matrix rotation and translation
%      0.68722163   0.68605906   0.23885018 -177.23571777
%     -0.70449696   0.54918799   0.44952930 199.36660182
%      0.17723000  -0.47719548   0.86074038 136.83803329
%   Axis  -0.55419404   0.03684971  -0.83157144
%   Axis point  88.14428764 305.16525298   0.00000000
%   Rotation angle (degrees)  56.73069329
%   Shift along axis  -8.22102053


