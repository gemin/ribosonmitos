% Function that converts a stopgap/tomo_sa/dynamo motive list.em into a .star file that can be opened in the Blender plugin called "Molecular Nodes" for visualization
% Euler angles need to be converted from ZXZ to ZYZ conventions.

function em2bdStar(baseName,starHdr)

% baseName='motl26mito7';
% starHdr='starHeader.star';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize output (o)
o=[baseName '-tom_zxz2zyz.star'];
% Read data
m=emread([baseName '.em']); 

% Format data as "proto-star"
M=[zeros([1,size(m,2)]);m(8,:);m(9,:);m(10,:);m(17,:);m(18,:);m(19,:);ones([1,size(m,2)])];

% Convert'em angles & place'em back in proto-star
a=M(5:7,:); b=zeros(size(a));
for i = 1:size(a,2) ; [~,e]=tom_eulerconvert_xmipp(a(1,i),a(2,i),a(3,i),'2star'); b(:,i)=e; end
M(5:7,:)=b;

% Write o.star
copyfile(starHdr,o);
f=fopen(o,'a'); fprintf(f,'%i %.6f %.6f %.6f %.6f %.6f %.6f %i\n',M); fclose(f); % ._f necessary as Blender crashes on integers
disp(o);

end
