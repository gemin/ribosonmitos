% Function to make sure that your random angles are different every time you (in case you take them from a randAngle.txt file) 
%%OGRandomAngleTriplets
%generates randAngle.txt

RandAngle=zeros(100000,3);
for i=1:length(RandAngle)
    RandAngle(i,1) = sign(rand()*2-1)*rand()*180.0;
    RandAngle(i,2) = sign(rand()*2-1)*rand()*180.0;
    RandAngle(i,3) = sign(rand()*2-1)*rand()*180.0;
end
csvwrite('randAngle.txt',RandAngle);