% Function to mask a motive list.csv using a 3D mask.h5 and output a masked motive list.csv

function h5xcsv2csv(IOFolder,TOMO,maskType,binFactor,ChkFlag,pad)

%% Variables %%
% IOFolder='MatlabTmp/' %Same folder for input and output.
% TOMO=0           % number of tomogram, interpreted as 3-digit number
% maskType='.mitoCrown2-30'  % Input name, HARDCODED as .h5 file
% binFactor=1       % Segmentation @bin16 VS picking @bin4 => binFactor=4
% ChkFlag=1       % Throw a window to verify masking happened
% pad=[0 0]         % pad=[0 16] resized mask to fit volume size= 912x944x304 in original implementation

%% Examples
% T=load('tomo.list'); for i=1:length(T) ; h5xcsv2csv('MatlabTmp/',T(i),'.cyto',4,1,[0,0]); end
% Continue with OGPickCsv2StarChunk('tomo.list','.cyto.txt','.cyto.starChunk.txt',0);
% then, in shell,
% cpy ~/WarpIniStarHeader.star tmp;
% for i in `cat tomo.list` ; do awk '{print "SEDME.mrc_16.60Apx.mrc "$0}' $i.cyto.starChunk.txt | sed "s/SEDME/$i/g" >> tmp ; done
% mv tmp 210215.cyto.star

% T=csvread('done.list'); IOFolder='MatlabTmp/'; maskType='.cyto-mito20'; binFactor=2 ; ChkFlag=0 ; pad=[212 0];
% for i=1:length(T); h5xcsv2csv(IOFolder,T(i),maskType,binFactor,ChkFlag,pad); end

% IOFolder='MatlabTmp/';tomo=36;maskType='.cyto-mito20';binFactor=2;ChkFlag=1;pad=[0,0];

%% Yallah habibi

tomo=sprintf(['%0' num2str(3) 'd'],TOMO);   %% HARD-CODED NtomoDigits=3.

% Load and shape .h5 mask
maskFile=strcat(IOFolder,tomo,maskType,'.h5');		% maskType in {.cyto .mito .void}
M=h5read(maskFile,'/data');	% gets the mask volume as a matrix
M=squeeze(M);
%M=imrotate(M,90);		% Different conventions in different programs => rotation required for cryoCebra (Napari)
M(M>1)= 1;              % All mitos born equal : different mito labels pooled for binary segmentation (voxels either considered mito or non-mito.)
mask=imresize3(M,binFactor);		% Correct mask binning.
%se=strel('sphere',4); QuaErDilM=imerode(imerode(imerode(imerode(imdilate(imdilate(imdilate(imdilate(mask,se),se),se),se),se),se),se),se);		% Quickly fills gaps and does not crash (if larger sphere)
padMask=uint16(padarray(QuaErDilM,pad,0,'pre')); % pad=[0 32] resized mask to fit volume size= 912x944x304 in original implementation

% Transform picking_output.csv motl into a .tif volume placing particle# at their xyz coordinates
dim=strcat(IOFolder,tomo,'.dim'); % was [944,912,304]
csv=strcat(IOFolder,tomo,'.csv');
output=strcat(IOFolder,tomo,'.NNpick.tif');
outputCsv=strcat(IOFolder,tomo,maskType,'.csv');
pickVol=OGcsvNum2tif(dim,csv,output);

%Zbug-fix
if size(pickVol,3) ~= size(padMask,3)    
    Zstart=round((size(pickVol,3)-size(padMask,3))/2);
    Zstop=Zstart+size(padMask,3)-1;
    tmp=zeros(size(pickVol)); tmp(:,:,Zstart:Zstop)=padMask; padMask=uint16(tmp);
end

% Apply mask 
maskedPickVol=padMask.*pickVol;
maskedPicks=unique(maskedPickVol);
maskedPicks=maskedPicks(2:end);		%Trim 0 out

% Extract masked particles and output .csv
iniCsv=csvread(csv);
newCsv=iniCsv(maskedPicks,:);
csvwrite(outputCsv,newCsv);

% Check output if ChkFlag up
if ChkFlag
	figure();
    Zmid=round(size(pickVol,3)/2);
	subplot(1,3,1); imshow(imdilate(squeeze(pickVol(:,:,Zmid)),se).*65530); %imshow(squeeze(pickVol(:,:,150)));
	subplot(1,3,2); imshow(squeeze(padMask(:,:,Zmid).*65530));
	subplot(1,3,3); imshow(imdilate(squeeze(maskedPickVol(:,:,Zmid)),se).*65530); %imshow(squeeze(maskedPickVol(:,:,150)));
	
	figure();
	imagesc(squeeze(padMask(:,:,Zmid).*65530),'AlphaData',1); axis equal; axis tight; axis off; colormap copper; hold on;
	imagesc(imdilate(squeeze(pickVol(:,:,Zmid).*65530),se),'AlphaData',0.5);
	imagesc(imdilate(squeeze(maskedPickVol(:,:,Zmid).*65530),se),'AlphaData',0.5);
end
end