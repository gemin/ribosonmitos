% Function to trim away particles with insufficient cross-correlation score to their reference map & split partcles per mitoGroup.

function MOTL = ccFilterDpdgOnFeature(basename,xlsFile,outputSuffix)
cc=xlsread(xlsFile);
MOTL=[];
for i=1:length(cc)
    motl=[basename,'_mito_',sprintf('%02d',cc(i,2)),'.em'];
    ccfilter(motl,cc(i,4));
    motl=[basename,'_mito_',sprintf('%02d',cc(i,2)),'-cc',num2str(cc(i,4)),'.em'];
    MOTL=[MOTL,emread(motl)];
end
emwrite(MOTL,[basename,outputSuffix]);
%'-ccFilteredDpdgOnFeature.em'
