% Function to trim away particles with insufficient cross-correlation score to their reference map.

function [newmotl] = ccfilter(motl,ccmin)  % provide cc x 1000 as input

if (ischar(motl))
    motlname=motl(1:end-3);
    motl=emread(motl);
else
    motlname='output';
end

newmotl=motl(:,motl(1,:)>ccmin/1000);
emwrite(newmotl,[motlname,'-cc',num2str(ccmin),'.em']);
