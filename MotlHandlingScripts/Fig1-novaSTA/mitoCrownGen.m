% Script that combines mitochondria segmentation and cytosol seg' masks.h5 for one tomogram and produces a cytosolic mask.h5 of the vicinity of mitochondria
% by dilating the mitoMask and returning the intersection of cyto and dilated-mito masks.

function mitoCrownGen(IOFolder,TOMO,erodeFactor,swellFactor)

%% Variables %%
% IOFolder=''; IOFolder='/g/mattei/0livier/220819-SP-poolD7/cytomitoCBPseg/210215/masks/' %Same folder for input and output.
% tomo=000           % number of tomogram, interpreted as 3-digit number
% erodeFactor=2     % mitoShrinking conservative masks - in pixels
% swellFactor=15	% mitoSwelling - in pixels

%% Example
% cd /home/gemin/HRseg/TS_000.mrc/ ;
% mitoCrownGen('',000,2,30);
%
% t=csvread('tomo.list'); for i = 1:length(t) ; mitoCrownGen('',t(i),2,15); end

%% Yallah habibi

tomo=sprintf(['%0' num2str(3) 'd'],TOMO);   %% HARD-CODED NtomoDigits=3.
output=strcat(IOFolder,tomo,'.mitoCrown',num2str(erodeFactor),'-',num2str(swellFactor),'.h5');

% Load and shape .h5 mask
%Cfile=strcat(IOFolder,tomo,'.cyto.h5');	% maskType in {.cyto .mito .void}
Mfile=strcat(IOFolder,tomo,'.mito.h5');
%C=h5read(Cfile,'/data');	% gets the mask volume as a matrix
H=h5info(Mfile);
M=h5read(Mfile,['/' H.Datasets.Name]);
%C=imrotate(C,90);			% Different conventions in different programs => rotation required for visualization
%M=imrotate(M,90);
%se=strel('sphere',4);
% Fills gaps in masks (quick and does not crash with a 'se' sphere of radius 4 ; it did for a larger se...)
% C=imerode(imerode(imerode(imerode(imdilate(imdilate(imdilate(imdilate(C,se),se),se),se),se),se),se),se);	
%%%%%%%%M=imerode(imerode(imerode(imerode(imdilate(imdilate(imdilate(imdilate(M,se),se),se),se),se),se),se),se); % Quickly fills gaps and does not crash (it does for a larger se sphere...)

% Inflate and invert mito mask
Mmax=M;Mmin=M;
se=strel('sphere',1);
for i = 1:swellFactor
    Mmax=imdilate(Mmax,se);
end
%Mmax=uint16(-(int16(Mmax)-1)); %invert mitoMask
Mmax=uint16(Mmax);

for i = 1:erodeFactor
    Mmin=imerode(Mmin,se);
end
Mmin=uint16(-(int16(Mmin)-1)); %invert mitoMask

% Crop cyto mask => cyto-mito20
Mout=Mmax.*Mmin;
%Cout=imrotate(Cout,-90);    
if ~isfile(output)
    h5create(output,'/data',size(Mout));
end
h5write(output,'/data',Mout);

end