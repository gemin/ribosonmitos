% Function to plot fsc from M's _fsc.star file

function []=fsc_warp(fscStarFileName,pxSz)
% fscStarFileName='w53-unfil_fsc.star'; pxSz=4.15;

text=fileread(fscStarFileName);
text=replace(text,'Infinity','99999999');
fsc=textscan(text(111:end),'%f %f %f %f %f');
figure, for i=2:5 ; fsc_visualize(fsc{i},pxSz,'',''); end
legend('wrpFSCUnmasked','wrpFSCRandomized','wrpFSCCorrected','wrpFSCMasked');
title(fscStarFileName)

end