% Function to convert csv files into proto-.star files for particle extraction in Warp/Relion.
% The outputs still need to be appended to a header.star file ; you can do it with this command :
% cpy ~/WarpIniStarHeader.star tmp; for i in `cat tomo.list` ; do awk '{print "TS_SEDME.mrc_13.48Apx.mrc "$0}' $i.cytoRA.txt | sed "s/SEDME/$i/g" >> tmp ; done
% mv tmp beautiful.cyto.star

function OGPickCsv2StarChunk(tomolist,iSfx,oSfx,RdmAngSwitch)
%% Inputs
% tomolist
% iSfx : input.csv suffix of segmentation mask processed with h5xcsv2csv.m
% oSfx : output.txt suffix then used in `awk` to concatenate motive lists for all tomograms **

%% Example
% OGPickCsv2StarChunk('tomo.list','.cyto.csv','.cytoRA.txt',1); %(RA:RandAngles)
% or OGPickCsv2StarChunk('one.list','.cyto-mito20.csv','.cyto-mito20.starChunk.txt',0);
% then,
% **
% cpy ~/WarpIniStarHeader.star tmp; for i in `cat tomo.list` ; do awk '{print "TS_SEDME.mrc_13.48Apx.mrc "$0}' $i.cytoRA.txt | sed "s/SEDME/$i/g" >> tmp ; done
% mv tmp 001-008.cyto.star

%% Function

tomolist=load(tomolist);
if RdmAngSwitch
    OGRandomAngleTriplets; %generates randAngle.txt
    RandomAngles=load('randAngle.txt');
end

for i=1:length(tomolist)
    tomo=sprintf(['%0' num2str(3) 'd'],tomolist(i));
    xyz=csvread(strcat(tomo,iSfx));
    if RdmAngSwitch
        ang=RandomAngles(1:length(xyz),:);
        tot=[xyz(:,8:10) ang];
    else
        tot=[xyz(:,8:10) xyz(:,17:19)];
    end
    writematrix(tot,strcat(tomo,oSfx),'Delimiter',' ');
end