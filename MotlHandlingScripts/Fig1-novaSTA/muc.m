%% muc.m
% muc stands for motl_update_coordinates (== transfer "full pixel" info from d(xyz) shifts to xyz subtomo coordinates.)

function new_motl = muc(motl)

if (ischar(motl))   %similar to motl_load
    motlname=motl(1:end-3);
    motl=emread(motl);
else
    motlname='output';
end

new_motl=motl;

shifted_coordinates=motl(8:10,:)+motl(11:13,:);
new_motl(8:10,:)=round(shifted_coordinates);
new_motl(11:13,:)=shifted_coordinates-new_motl(8:10,:);

emwrite(new_motl,[motlname,'-uc.em']);

end