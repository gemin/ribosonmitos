% Function that trims a motive list from positions that are within x/y/zCrop voxels away from tomo border.
% Dim lists tomo numbers in column 1, then xLim, yLim and zLim.

function OGMotlTrimxyzDim(motl,Dim,xCrop,yCrop,zCrop,output)
 
if(ischar(motl))
    m = emread(motl);
else
    m=motl;
end

new_motl=[];
for i = 1:size(Dim,1)
    xMax=Dim(i,2)-xCrop;
    yMax=Dim(i,3)-yCrop;
    zMax=Dim(i,4)-zCrop;
    Temp=m(:,m(5,:)==Dim(i,1));
    Temp=Temp(:,Temp(8,:)>xCrop);
    Temp=Temp(:,Temp(9,:)>yCrop);
    Temp=Temp(:,Temp(10,:)>zCrop);
    Temp=Temp(:,Temp(8,:)<xMax);
    Temp=Temp(:,Temp(9,:)<yMax);
    Temp=Temp(:,Temp(10,:)<zMax);
    new_motl=[new_motl,Temp];
 end
emwrite(new_motl,output);


