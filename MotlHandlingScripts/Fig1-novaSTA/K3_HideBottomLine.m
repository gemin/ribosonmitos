% Function to crop away artifacted lines in images.tif output by the K3 camera of EMBL-SCB-Krios2.

%% K3_HideBottomLine.m
%
% Variables:
% file : "gold" mask to edit. They are binned 4x by default in Warp.
% prefix : for output name. '' to overwrite
% LineWidth : 5 looks reasonable in case more than 1 line is faulty on K3
%
% Use:
% cd .../frames/mask;
% d=dir;
% for i = 3:length(d) ; K3_HideBottomLine(d(i).name,'',5); end

function K3_HideBottomLine(file,prefix,LineWidth)
% Read mask & hide bottom line
M=imread(file);
M(end-LineWidth:end,:)=1;
%
t=Tiff(strcat(prefix,file),'w');
setTag(t,'Photometric',Tiff.Photometric.MinIsBlack);
setTag(t,'ImageLength',size(M,1)); setTag(t,'ImageWidth',size(M,2));
setTag(t,'PlanarConfiguration',Tiff.PlanarConfiguration.Chunky);
setTag(t,'BitsPerSample',8);
write(t,uint8(M));
close(t)
end