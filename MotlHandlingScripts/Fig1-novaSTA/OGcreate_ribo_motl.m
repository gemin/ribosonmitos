% Function that takes sphere models.mod in input and outputs a motive list in which the surface of input spheres were oversampled
% with points for subtomo initial extaction. Orientations set to random values (see lines 70-73).

function OGcreate_ribo_motl(list_file,motl_folder,motl_file,mod_folder,tomo_digits)
%% Creates one motl file from all *.mod files
% list_file: list of tomograms
% motl_folder: path to the folder when the final motl will be saved
% motl_file: name of the motl to be created
% mod_folder: folder where all the mod files are stored
% tomo_digits: number of digits used for naming tomograms and mod files
% (i.e. for 090.rec there should be 090.mod file and the tomo_digits would
% be 3 in this case)

% Debug setup:
% list_file = 'tomo_list.txt';
% motl_folder = './motl';
% motl_file = 'motl_new.em';
% mod_folder = 'clicker/';
% tomo_digits=3;
% = OGcreate_ribo_motl('tomo.list','./mod-files/','motl_new.em','mod-files/',3);

if exist(motl_folder, 'dir') ~= 7
	system(['mkdir ' motl_folder]);
end

tomo_numbers=dlmread(list_file);
tomo_numbers=unique(tomo_numbers);
count_pts = 1;

motl=[];

for i = 1:numel(tomo_numbers)	  
   
    tomo_str = sprintf(['%0',num2str(tomo_digits),'d'],(tomo_numbers(i)));
    mod_filename=strcat(mod_folder,'/',tomo_str,'.mod');
    coord_filename=strcat(mod_folder,'/',tomo_str,'_model.txt');
    
    %system(['model2point -object ' mod_file_name ' ' mod_file_name '.txt']);
    %coord=dlmread([ mod_file_name '.txt']);
    
    system(['model2point -object ' mod_filename ' ' coord_filename ' > NUL']);
    %system(['model2point -object ' mod_filename ' ' coord_filename]);
    coord=dlmread(coord_filename);
    
    number_of_views = unique(coord(:,1));
    
    for v=1:numel(number_of_views)
        
        current_view=coord(find(coord(:,1)==number_of_views(v)),:);
        number_of_pores=unique(current_view(:,2));
        
        %if(number_of_views(v)==1)
        correct_number_of_points = 1;
        %else
        %    correct_number_of_points = 1;
        %end
        
        for p=1:numel(number_of_pores)
            
            % Create new motl
            nm = zeros(20,1);
            
            % side, top, or bottom 
            %nm(2,1) = number_of_views(v);
            
            % tomo number
            nm(5,1) = tomo_numbers(i);
            nm(7,1) = tomo_numbers(i);
            
            % Phi, Psi, Theta randomized and same for all views (reverse order than actual computation : theta, psi, phi)
            nm(17,1) = sign(rand()*2-1)*rand()*180.0;
            nm(18,1) = sign(rand()*2-1)*rand()*180.0;
            nm(19,1) = sign(rand()*2-1)*rand()*180.0;
            
            % get all points relevant to the currently processed pore
            current_pore=current_view(find(current_view(:,2)==number_of_pores(p)),:);
            
            if(size(current_pore,1)~=correct_number_of_points)
                display('WARNING: ');
                display(['The number of points for the contour number ' num2str(number_of_pores(p)) ' (object number ' num2str(number_of_views(v)) ') in tomogram ' tomo_str ' is incorrect!']);
                display('Skipping this point!');
                continue;
            end
                
            nm(8:10,1)=current_pore(1,3:5);          
            motl = [ motl nm ];
            
        end
        
    end

end

% Set pore number in motive list
motl(4,:) = 1:size(motl, 2);
motl(6,:) = 1:size(motl, 2);
motl(20,:) = 1;

emwrite(motl, [motl_folder '/' motl_file]);

