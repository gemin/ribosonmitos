# RibosOnMitos

Hello,

This repository aims at summing up the files I used to analyze data for this to-be publication:

**Hibernating ribosomes tether to mitochondria as an adaptive response to cellular stress during glucose depletion**

Olivier Gemin1†, Maciej Gluc2†, Michael Purdy2, Higor Rosa1, Moritz Niemann1, Yelena Peskova2, Simone Mattei1,3*, Ahmad Jomaa2,4*

**One sentence summary:** Yeast cells adapt to glucose depletion by tethering hibernating ribosomes to mitochondria, promoting survival.

.

Said files are organized by purposes:

• Scripts for handling motive lists (motl, i.e. coordinates and orientations of subtomograms)
   - MATLAB scripts inherited from Beata Turonova (novaCTF, novaSTA) can be found here: https://git.embl.de/turonova/tomo_sa
   - original MATLAB scripts for cryo-ET data processing (e.g. seeding spheres with 6D normals: mitoOversampling.m)
   - bash scripts or workflows to torture motls (e.g. file format conversion: em2star.m)

• Scripts for image analysis
   - python script to quickly pick big particles (e.g. ribosomes) in binned tomograms
   - maybe stg on ice crystallinity quantitation (work in progress)

• .blend files used for 3D visualization in Blender. 

.

I hope you'll find useful stuff in there ! Feel free to use it as you please.

All the best,

Olivier
