##From scratch: first set up environment
#module load Mamba
#mamba init
# Cut new lines added to ~/.bashrc and paste them in ~/mambaInit.sourceMe
#source ~/mamba.sourceMe
#mamba create -n env01
#mamba activate env01
#mamba install numpy scipy scikit-image
#pip install mrcfile

import glob
import mrcfile
import numpy as np
from scipy.ndimage import gaussian_filter,binary_dilation,binary_erosion,median_filter
from skimage.measure import block_reduce #scikit-image module
from skimage.morphology import skeletonize_3d
import csv
import os

#tomolist=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81)
tomolist=(1,2)

tomoPath='./'
outPath='blobPks/'
chkPath='tmp/'

QchkFlag=1 # Kwaliti czech

tomoDim=(512,512,500) # Declare tomo size. Make sure it fits with tomos.tomo_size[i] in the .tomostxt files.

trimVal=30  # XY border trim, in binned pixels
ZtrimVal=50 # idem for Z

# Parameters to tweak to limit false posi/negatives
S = 2  # Shrink factor
g1 = 4 # cutoff 1
g2 = 7 # cutoff 2
thr= -0.15 # Skeletonizing threshold

clnDistFlag=1 # Remove 3D-clustered particles (picking being done in 2D here, that's useful.)
clnDistThr=7 # ProxiThreshold in binned px

modSphSize = 5 # size of sphere displayed in 3dmod (only impacts the suggested command line printed in the terminal once picking is done.)

##########################################################
# ClnDist function
def filterDist(data,condition):
 result = []
 for element in data:
  if all(condition(element,other) for other in result):
   result.append(element)
 return result

def quad_condition(xs,ys):
 return sum((x-y)*(x-y) for x,y in zip(xs,ys)) > clnDistThr*clnDistThr
##################

Xmin=trimVal ; Xmax=tomoDim[0]-trimVal
Ymin=trimVal ; Ymax=tomoDim[1]-trimVal
Zmin=ZtrimVal ; Zmax=tomoDim[2]-ZtrimVal

pts = np.zeros((0,3),np.float32) # Coordinates
tid = np.zeros((0,1),np.uint32)  # tomoID list

for i in tomolist:
        tomoName = tomoPath+'%03i.mrc'%i #####  FILL IN TOMO_NAME TEMPLATE 
        with mrcfile.open(tomoName) as mrc:
            v=mrc.data
        v   = (v-v.mean())/v.std() # normalize
        v   = gaussian_filter(v,g1) # blur
        v   = v - gaussian_filter(v,g2) # bandpass
        vv  = block_reduce(v,(S,S,S),np.min) # skimage.measure.block_reduce ~ binning. No effect if S=1.
        vvg = skeletonize_3d((vv<thr))
        tmp = np.argwhere(vvg>0) # get coordinates of all ones. tmp.shape = (N,3)
        print( 'Tomo %2d: %6d'%(i,tmp.shape[0])) #chk
        pts = np.vstack((pts,tmp)) # list pts as x,y,z coordinates << increasing in size at each iteration
        tid = np.vstack((tid,i*np.ones((tmp.shape[0],1)))) # fill tid($i) with a list of $i, the size of tmp
        ccv = gaussian_filter(np.float32(vvg),1) # blur skeleton
        if QchkFlag==1:
            with mrcfile.new(chkPath+'%03i-1-v_g%i-g%i.rec'%(i,g1,g2),overwrite=True) as mrc:
                mrc.set_data(np.float32(v))
            with mrcfile.new(chkPath+'%03i-2-vv_blkRed%i.rec'%(i,S),overwrite=True) as mrc:
                mrc.set_data(np.float32(vv))   
            with mrcfile.new(chkPath+'%03i-3-vvg_skel3D%i.rec'%(i,thr),overwrite=True) as mrc:
                mrc.set_data(np.float32(vvg))
            with mrcfile.new(chkPath+'%03i-4-ccv_skel3D-blur1.rec'%i,overwrite=True) as mrc:
                mrc.set_data(np.float32(ccv))

        pts = S*pts + S/2 # Un-shrink coordinates
        ix  = (pts[:,0]>Zmin) & (pts[:,1]>Ymin) & (pts[:,2]>Xmin) & (pts[:,0]<Zmax) & (pts[:,1]<Ymax) & (pts[:,2]<Xmax) # Trim tomo borders (XZ swapped for some reason)
        pts = pts[ix>0] ; tid = tid[ix>0]
        print(pts.shape) #chk
        modname='tomo%i'%i     #####  FILL IN output MOD_NAME TEMPLATE
        #mask=tid==i
        #p = pts[mask.flatten(),:]
        pos = np.vstack( (pts[:,2],pts[:,1],pts[:,0]) ).transpose()
        print(modname,pos.shape)
    
        if clnDistFlag==1:
            posCLN = np.array(filterDist(pos,quad_condition))
            print(posCLN.shape)
            pos=posCLN
            modname='tomo%i-CLN%c'%(i,clnDistThr)
        
        np.savetxt(outPath+modname+'.txt',pos)
        print('checkable in IMOD after : point2model -in '+outPath+modname+'.txt -ou '+outPath+modname+'.mod -sc -sp %i'%modSphSize)
        print('3dmod '+chkPath+'%03i-1-v_g%i-g%i.rec '%(i,g1,g2)+outPath+modname+'.mod')
 
print('Done!')
